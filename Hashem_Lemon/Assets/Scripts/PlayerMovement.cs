﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float turnSpeed = 20f;

    Animator m_Animator;
    Rigidbody m_Rigidbody;
    AudioSource m_AudioSource;
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;

    bool haveMelee = false;

    public int health = 3;
    bool invincible = false;
    public GameEnding gameEnding;
    public Transform hitBox;

    public Material invmat;
    public Material[] origmats;
    public SkinnedMeshRenderer smr;
    public CanvasGroup introText;

    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_AudioSource = GetComponent<AudioSource>();

        origmats = new Material[smr.materials.Length];
        smr.materials.CopyTo(origmats, 0);
    }

    private void Update()
    {
        if (haveMelee)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Collider[] hitColliders = Physics.OverlapBox(hitBox.transform.position, hitBox.transform.localScale, hitBox.transform.rotation);

                for (int i = 0; i < hitColliders.Length; i++)
                {
                    if (hitColliders[i].gameObject.CompareTag("Enemy"))
                    {
                        EnemyHealth eHealth = hitColliders[i].gameObject.GetComponent<EnemyHealth>();

                        if (eHealth != null)
                            eHealth.TakeDamage(1);
                    }
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Weapon"))
        {
            haveMelee = true;

            print("Weapon on");

            Destroy(other.gameObject);
        }

        if (other.gameObject.CompareTag("Pill"))
        {
            invincible = true;
            haveMelee = true;

            smr.materials = new Material[] { invmat, invmat };
            print("invincible");
            Invoke("EndInvincible", 20f);
            Invoke("EndWeapons", 20f);


            Destroy(other.gameObject);
        }
    }

    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize();

        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;
        m_Animator.SetBool("IsWalking", isWalking);

        if (isWalking)
        {
            if (!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            m_AudioSource.Stop();
        }

        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation(desiredForward);
    }

    void OnAnimatorMove()
    {
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude);
        m_Rigidbody.MoveRotation(m_Rotation);
    }

    public void TakeDamage(int damage)
    {
        if (health > 0 && !invincible)
        {
            health -= damage;

            invincible = true;
            Invoke("EndInvincible", .5f);

            if (health <= 0)
            {
                gameEnding.CaughtPlayer();
            }
        }
    }

    void EndInvincible()
    {
        invincible = false;
        smr.materials = origmats;
        print("all done");
    }

    void EndWeapons()
    {
        haveMelee = false;
    }
}